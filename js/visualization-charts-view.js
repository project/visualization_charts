/**
 * @file
 * Provides options for chart visualization.
 */

(function ($) {
  Drupal.behaviors.VisualizationEntityChartsView = {
    attach(context) {
      const isIframe =
        !document.querySelector('.content').offsetWidth &&
        !document.querySelector('.content').offsetHeight;
      let state = document.querySelector(
        '.field--name-chart-settings .field__item',
      ).textContent;
      let $el;
      let title;
      let height;
      let $body;
      let $window;

      function cleanURL(url) {
        const haveProtocol = new RegExp('^(?:[a-z]+:)?//', 'i');
        if (haveProtocol.test(url)) {
          url = url.replace(haveProtocol, '//');
        }
        return url;
      }

      function getChartHeight(hasTitle) {
        const height = !hasTitle
          ? $(window).height()
          : $(window).height() - $body.find('h2.chartTitle').outerHeight(true);

        return height;
      }

      function resize() {
        const $title = $body.find('h2.chartTitle');
        const hasTitle = !!$title.length;
        $title.style.marginTop = '0px';
        $title.style.padding = '20px';
        $title.style.marginBottom = '0px';
        const height = getChartHeight(hasTitle);
        $('.recline-nvd3').height(height);
        $('#iframe-shell').height(height);
      }

      if (state) {
        state = new recline.Model.ObjectState(JSON.parse(state));
        $body = $(document.body);
        $window = $(window);
        $body.removeClass('admin-menu');

        if ($('#iframe-shell').length) {
          $el = $('#iframe-shell');
          if (state.get('showTitle')) {
            title = $el.find('h2 a').html();
            $body.prepend(`<h2 class="chartTitle">${title}</h2>`);
            height = getChartHeight(true);
            resize();
          } else {
            height = getChartHeight(false);
          }
          state.set('height', height);
          state.set('width', $window.width() - 10);
          $window.on('resize', resize);
        } else {
          $el = $('#graph');
          state.set('width', $('.field--name-chart-settings').width());
        }

        const source = state.get('source');
        let graph = null;
        source.url = cleanURL(source.url);
        const model = new recline.Model.Dataset(source);
        state.set('model', model);
        state.get('model').queryState.attributes = state.get('queryState');

        graph = new recline.View.nvd3[state.get('graphType')]({
          model,
          state,
          el: $el,
        });

        model
          .fetch()
          .done(graph.render.bind(graph))
          .fail(function (err) {
            console.log(err);
            alert('Failed to fetch the resource');
          });
      }
    },
  };
})(jQuery);

/**
 * @file
 * Provides options for chart visualization.
 */

(function ($) {
  let sharedObject;

  Drupal.behaviors.VisualizationEntityCharts = {
    attach() {
      const currentState = document.getElementById(
        'edit-chart-settings-0-value',
      ).value;
      let state;
      let model;

      function cleanURL(url) {
        const haveProtocol = new RegExp('^(?:[a-z]+:)?//', 'i');
        if (haveProtocol.test(url)) {
          url = url.replace(haveProtocol, '//');
        }
        return url;
      }

      function setActiveStep(n) {
        const $stages = $('#ve-chart-form .stages li');
        $stages.removeClass('active');
        $stages.eq(n).addClass('active');
      }

      function init() {
        const msv = new MultiStageView({
          state,
          el: $('#steps'),
        });

        msv.addStep(new LoadDataView(sharedObject));
        msv.addStep(new DataOptionsView(sharedObject));
        msv.addStep(new ChooseChartView(sharedObject));
        msv.addStep(new ChartOptionsView(sharedObject));

        msv.on('multistep:change', function (e) {
          setActiveStep(e.step);
        });
        msv.render();

        // Revert to Step 1 when the Source File is changed.
        /* eslint-disable-next-line no-jquery/no-ajax-events */
        $(document).ajaxComplete(function (e, xhr, settings) {
          if (
            settings.url &&
            settings.url.search('element_parents=source_file/widget') !== -1
          ) {
            const url = $('.field--name-source-file a').prop('href');
            const backend = document.getElementById(
              'control-chart-backend',
            ).value;
            let delimiterValue = ',';
            const selectElement = document.getElementById(
              'control-source-delimiter',
            );
            if (selectElement) {
              const delimiter =
                selectElement.options[selectElement.selectedIndex].value;
              const delimiterMapping = {
                comma: ',',
                semicolon: ';',
                pipe: '|',
                space: ' ',
              };
              delimiterValue = delimiterMapping[delimiter];
            }
            const source = {
              backend,
              url,
              delimiter: delimiterValue,
            };
            sharedObject.state.set('source', source);
            msv.gotoStep(0);
            msv.render();
          }
        });
        const $resourceField = $('.field--name-source-file input');

        $resourceField.on('change', function () {
          const url = $('.field--name-source-file a').attr('href');
          document.getElementById('control-chart-source').value = url;
        });
        sharedObject.state.on('change', function () {
          document.getElementById('edit-chart-settings-0-value').value =
            JSON.stringify(sharedObject.state.toJSON());
        });
        window.msv = msv;
        window.sharedObject = sharedObject;
      }

      // There is not saved state. Neither database nor memory.
      if (currentState && !sharedObject) {
        state = new recline.Model.ObjectState(JSON.parse(currentState));
        model = state.get('model');
        if (model && !model.records) {
          // Ensure url is protocol agnostic
          model = state.get('model');
          model.url = cleanURL(model.url);
          model = new recline.Model.Dataset(model);

          // Hack: check if the file exists before fetch.
          // CSV.JS does not return an ajax promise then
          // we can't know if the request fails.
          $.get(state.get('model').url)
            .done(function () {
              model.fetch().done(init);
              state.set('model', model);
              state.get('model').queryState.attributes =
                state.get('queryState');
              sharedObject = { state };
            })
            .fail(function () {
              sharedObject = { state };
              sharedObject.state.set({ step: 0 });
              init();
            });
        } else if (model && model.records) {
          state.set('model', new recline.Model.Dataset(model));
          state.get('model').queryState.attributes = state.get('queryState');
          sharedObject = { state };
          init();
        }
      } else if (!sharedObject) {
        state = new recline.Model.ObjectState();
        state.set('queryState', new recline.Model.Query());
        sharedObject = { state };
        init();
      } else if (currentState) {
        state = new recline.Model.ObjectState(JSON.parse(currentState));
      }

      if (state) {
        setActiveStep(state.get('step'));
      } else {
        setActiveStep(0);
      }
    },
  };
})(jQuery);

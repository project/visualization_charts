<?php

namespace Drupal\visualization_charts\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the visualization charts entity edit forms.
 */
class VisualizationChartsForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    $entity = $this->getEntity();

    $message_arguments = ['%label' => $entity->toLink()->toString()];
    $logger_arguments = [
      '%label' => $entity->label(),
      'link' => $entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New visualization chart %label has been created.', $message_arguments));
        $this->logger('visualization_charts')->notice('Created new visualization chart %label', $logger_arguments);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The visualization chart %label has been updated.', $message_arguments));
        $this->logger('visualization_charts')->notice('Updated visualization chart %label.', $logger_arguments);
        break;
    }

    $form_state->setRedirect('entity.visualization_charts.canonical', ['visualization_charts' => $entity->id()]);

    return $result;
  }

}

<?php

namespace Drupal\visualization_charts;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a visualization charts entity type.
 */
interface VisualizationChartsInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}

# Visualization Charts

The Visualization Charts is a Drupal 8/9 module that allows you to create
interactive visualization using CSV data sources. It enables content creators
to provide more meaning to raw data, illustrate trends and engage users.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/visualization_charts).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/visualization_charts).


## Table of contents

- Requirements
- Installation
- Configuration
- Troubleshooting
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module at Administration > Extend.
2. Go to /visualization-charts/add and start creating visualization charts.


## Troubleshooting

If the Visualization Charts menu does not display, check the following:

- Are the "Access administration menu" and "Use the administration pages and
  help" permissions enabled for the appropriate roles?


## MAINTAINERS

 * Vishal Kadam (vishal.kadam) - https://www.drupal.org/user/3622517
